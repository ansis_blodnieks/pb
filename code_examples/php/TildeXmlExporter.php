<?php

namespace App\Services\Invoices\Export;


use App\Models\Refr\CrmGeneratedInvoiceExport;
use App\Services\Invoices\Enums\InvoiceType;
use App\Services\Invoices\ExportService;

class TildeXmlExporter extends ExportService
{
    /**
     * @return CrmGeneratedInvoiceExport
     * @throws \Throwable
     */
    public function generate(): CrmGeneratedInvoiceExport
    {
        $tmpfile = tempnam('/tmp', 'invoice_tilde_export');

        $invoices = $this->getInvoicesByDataIds($this->distributorId, $this->invoiceDataIds, [
            InvoiceType::DISTRIBUTOR,
            InvoiceType::COMPANY,
            InvoiceType::CREDIT,
        ]);

        $html = view('crm.invoices.new.tilde_xml', [
            'invoices' => $invoices,
        ])->render();

        file_put_contents($tmpfile, $html);

        $generatedInvoice = new CrmGeneratedInvoiceExport();
        $generatedInvoice->distributor_id = $this->distributorId;
        $generatedInvoice->is_temp = 1;
        $generatedInvoice->mime = 'text/xml';
        $generatedInvoice->filename = $tmpfile;
        $generatedInvoice->download_filename = $this->getFilename($invoices, 'xml');
        $generatedInvoice->save();

        return $generatedInvoice;
    }
}
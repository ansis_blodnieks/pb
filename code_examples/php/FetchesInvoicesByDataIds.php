<?php

namespace App\Services\Invoices\Traits;


use App\Models\Refr\CompanyInvoice;
use App\Models\Refr\CreditInvoice;
use App\Models\Refr\DistributorInvoice;
use App\Models\Refr\OrderInvoice;
use App\Services\Invoices\Enums\InvoiceType;

trait FetchesInvoicesByDataIds
{
    /**
     * @param int $distributorId
     * @param array $invoiceDataIds
     * @param array $invoiceTypes
     * @return CompanyInvoice[]|CreditInvoice[]|DistributorInvoice[]|OrderInvoice[]
     */
    public function getInvoicesByDataIds(int $distributorId, array $invoiceDataIds = [], array $invoiceTypes = []): array
    {
        if (empty($invoiceDataIds)) {
            return [];
        }

        $mapping = [
            InvoiceType::ORDER => [
                'class' => OrderInvoice::class,
                'table' => 't_xxx',
            ],
            InvoiceType::COMPANY => [
                'class' => CompanyInvoice::class,
                'table' => 't_xxx',
            ],
            InvoiceType::CREDIT => [
                'class' => CreditInvoice::class,
                'table' => 't_xxx'
            ],
            InvoiceType::DISTRIBUTOR => [
                'class' => DistributorInvoice::class,
                'table' => 't_xxx',
            ],
        ];

        $implodedDataIds = implode(',', array_map('intval', $invoiceDataIds));

        $sql = [];
        foreach ($mapping as $invoiceType => $invoiceTypeProps) {
            if (in_array($invoiceType, $invoiceTypes)) {
                $sql[] = "(
                      SELECT i.id, '{$invoiceType}' as `type` FROM {$invoiceTypeProps['table']} i
                      WHERE i.invoice_data_id IN ({$implodedDataIds})
                      AND created_by_distributor_id = {$distributorId}
                  )";
            }
        }

        if (empty($sql)) {
            return [];
        }

        $sql = implode("\nUNION\n", $sql);

        return array_map(function(\stdClass $invoice) use ($mapping) {
            return $mapping[$invoice->type]['class']::with(['data'])->find($invoice->id);
        }, \DB::select($sql));
    }
}
<?php

namespace App\Services\Invoices;


use App\Models\Refr\CompanyInvoice;
use App\Models\Refr\CreditInvoice;
use App\Models\Refr\CrmGeneratedInvoiceExport;
use App\Models\Refr\DistributorInvoice;
use App\Models\Refr\OrderInvoice;
use App\Services\Invoices\Traits\FetchesInvoicesByDataIds;

abstract class ExportService
{
    use FetchesInvoicesByDataIds;

    const TYPE_TILDE = 'tilde';
    const TYPE_PDF = 'pdf';
    const TYPE_XLSX = 'xlsx';

    protected $distributorId;
    protected $invoiceDataIds;

    public function __construct(int $distributorId, array $invoiceDataIds)
    {
        $this->distributorId = $distributorId;
        $this->invoiceDataIds = $invoiceDataIds;
    }

    /**
     * @param CompanyInvoice[]|CreditInvoice[]|DistributorInvoice[]|OrderInvoice[] $invoices
     * @param string $extension
     * @return string
     */
    protected function getFilename(array $invoices, string $extension): string
    {
        $invoiceCount = count($invoices);

        if (!$invoiceCount || $invoiceCount > 1) {
            return 'Invoices_export_' . date('Y-m-d_H_i_s') . '.' . $extension;
        }

        $invoiceNumber = str_replace('/', '_', $invoices[0]->invoiceNumber) . '.' . $extension;

        return $invoiceNumber;
    }

    abstract public function generate(): CrmGeneratedInvoiceExport;
}
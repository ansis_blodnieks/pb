import React, {Component} from 'react';
import Pagination from './Pagination'
import {listButtonTypes} from '../../../helpers';
import buttons from '../buttons'

class Table extends Component {

    onCheckAll(e) {
      this.props.onSelectAll();
      e.target.blur && e.target.blur();
    }

    render() {

        if (this.props.isLoading) {
            return <div style={{textAlign: 'center'}}>
                <img style={{
                    width: '70px',
                    height: '70px',
                    marginTop: '50px',
                    marginBottom: '50px',
                }} src="/public/loader.gif" />
            </div>
        }

        return (
            <div>

                <table className={`table`}>
                    <thead>
                    <tr>
                        <th>
                            <button type={`button`} onClick={(e) => this.onCheckAll(e)} className={`btn btn-default`}>
                              <i className={`fa fa-check`} />
                            </button>
                        </th>
                        { this.props.columns.map(column => <th style={{lineHeight: '32px'}} key={column.value}>{column.value}</th>) }
                        <th />
                    </tr>
                    </thead>
                    <tbody>
                    { this.props.pagination.currentChunk.map(row => <tr key={row.id}>
                        <td>
                            <div className="checkbox" style={{textAlign: 'center'}}>
                                <label>
                                    <input checked={this.props.selectedInvoices.indexOf(row.data_id) > -1}
                                           type="checkbox"
                                           value={row.data_id}
                                           onChange={(e) => this.props.onSelect(parseInt(e.target.value))}
                                    />
                                </label>
                            </div>
                        </td>
                        { this.props.columns.map(column => {
                            let value = row[column.value];
                            if (value && column.post !== undefined) {
                                value += ` ${row[column.post]}`
                            }
                            if (!value) {
                                value = '';
                            }
                            return <td key={column.value}>{`${value}`}</td>
                        }) }
                        <td style={{width: '200px', textAlign: 'right'}}>
                            <div className={`btn-group`}>
                                { row.buttons.map((button, i) => {

                                    const C = buttons[button.type] || null;

                                    switch (button.type) {
                                        case listButtonTypes.view:
                                        case listButtonTypes.send:
                                        case listButtonTypes.mark_as_part_paid:
                                        case listButtonTypes.mark_as_paid:
                                        case listButtonTypes.download:
                                        case listButtonTypes.multi:
                                            return <C key={`btn-${button.type}-${row.data_id}-${i}`}
                                                      onDownloadClick={this.props.onDownloadClick}
                                                      button={button}
                                                      dataId={row.data_id}
                                            />;
                                    }
                                }) }
                            </div>
                        </td>
                    </tr>) }
                    { !this.props.pagination.currentChunk.length && <tr>
                        <td colSpan={this.props.columns.length}>TRANS: No results..</td>
                    </tr> }
                    </tbody>
                </table>



              <div className={`row`}>
                <div className={`col-md-10`}>
                  <Pagination {...this.props.pagination} onClick={this.props.onPageChange} />
                </div>
                <div className={`col-md-2`}>
                  <select style={{
                    margin: '20px 0',
                  }} value={this.props.pagination.pageSize} className={`form-control`} onChange={(e) => this.props.onPageSizeChange(e.target.value)}>
                    { [20,50,100].map(pageSize => <option key={`ps-${pageSize}`} value={pageSize}>{pageSize}</option>) }
                  </select>
                </div>
              </div>

            </div>
        );
    }
}

export default Table;
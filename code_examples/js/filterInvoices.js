import _ from "lodash";
import moment from 'moment';
import {filterTypes} from '../../../helpers'

function filterInvoices (viewFilters = [], filters = [], invoices = []) {

    let filtersWithValues = [];

    for (let i = 0; i < viewFilters.length; i++) {

        let viewFilter = viewFilters[i];

        switch (viewFilter.type) {
            case filterTypes.text:
                if (filters[viewFilter.value] !== '') {
                    filtersWithValues.push(viewFilter);
                }
                break;
            case filterTypes.multi_select:
                if (filters[viewFilter.value].length > 0) {
                    filtersWithValues.push(viewFilter);
                }
                break;

            case filterTypes.number_range:
                if (filters[`${viewFilter.value}_from`] !== '' || filters[`${viewFilter.value}_to`] !== '') {
                    filtersWithValues.push(viewFilter);
                }
                break;

            case filterTypes.date_range:

                if (filters[`${viewFilter.value}_from`] !== null || filters[`${viewFilter.value}_to`] !== null) {
                    filtersWithValues.push(viewFilter);
                }
                break;
        }
    }

    return _.filter([...invoices], (invoice) => {

        let rowPasses = true;

        for (let i = 0; i < filtersWithValues.length; i++) {

            let passes = false;
            let viewFilter = filtersWithValues[i];
            const filterValue = filters[viewFilter.value];

            switch (viewFilter.type) {
                case filterTypes.text:
                    passes = invoice[viewFilter.value].toLowerCase().indexOf(filterValue) > -1;
                    break;

                case filterTypes.multi_select:
                    let filteredMS = filterValue.filter(v => {
                        return v.value === invoice[viewFilter.value]
                    });
                    passes = filteredMS.length > 0;
                    break;

                case filterTypes.number_range:

                    let numFrom = filters[`${viewFilter.value}_from`];
                    let numTo = filters[`${viewFilter.value}_to`];

                    if (numFrom && numTo) {
                        passes = parseFloat(invoice[viewFilter.value]) >= parseFloat(numFrom) && parseFloat(invoice[viewFilter.value]) <= parseFloat(numTo);
                    } else if (numFrom) {
                        passes = parseFloat(invoice[viewFilter.value]) >= parseFloat(numFrom);
                    } else { // numTo
                        passes = parseFloat(invoice[viewFilter.value]) <= parseFloat(numTo)
                    }

                    break;

                case filterTypes.date_range:

                    // if (viewFilter.value == 'sentAt') {
                    //     console.log(invoice[viewFilter.value])
                    // }

                    if (!invoice[viewFilter.value]) {
                        passes = false;
                    } else {

                        let dateFrom = filters[`${viewFilter.value}_from`];
                        if (dateFrom) {
                            dateFrom = dateFrom.startOf('day');
                        }
                        let dateTo = filters[`${viewFilter.value}_to`];
                        if (dateTo) {
                            dateTo = dateTo.endOf('day');
                        }

                        let rowDate = moment(invoice[viewFilter.value], "YYYY-MM-DD HH:mm:ss");

                        if (dateFrom && dateTo) {
                            passes = rowDate.isSameOrAfter(dateFrom) && rowDate.isSameOrBefore(dateTo);
                        } else if (dateFrom) {
                            passes = rowDate.isSameOrAfter(dateFrom);
                        } else { // dateTo
                            passes = rowDate.isSameOrBefore(dateTo)
                        }
                    }

                    break;
            }

            rowPasses = rowPasses && passes;
        }

        return rowPasses;
    });
}

export default filterInvoices;
<?php

require __DIR__ . '/../vendor/autoload.php';

if (isset($argv[1])) {
    define('NUMBER', (int)$argv[1]);
} else {
    define('NUMBER', 2);
}

if (isset($argv[2])) {
    define('STRING', (string)$argv[2]);
} else {
    define('STRING', "abcdef");
}

try {

    $input = new \App\Input();
    $input->init(NUMBER, STRING);

    $task = new \App\Task($input);
    $task->execute();

    print_r($task->getChunks());

} catch (\Exception $e) {

    echo 'Error: ' . $e->getMessage() . PHP_EOL;
}
<?php

namespace App\Exceptions;


use Throwable;

class InputNotInitializedException extends \Exception
{
    public function __construct(string $message = "Input is not initialized", int $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
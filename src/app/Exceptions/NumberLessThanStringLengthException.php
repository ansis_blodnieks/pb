<?php

namespace App\Exceptions;


use Throwable;

class NumberLessThanStringLengthException extends \Exception
{
    public function __construct(string $message = "Number must be greater than string length", int $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
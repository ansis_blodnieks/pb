<?php

namespace App\Exceptions;


use Throwable;

class InvalidStringException extends \Exception
{
    public function __construct(string $message = "String must consist of at least 1 character", int $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
<?php

namespace App\Exceptions;


use Throwable;

class InvalidNumberException extends \Exception
{
    public function __construct(string $message = "Invalid number entered", int $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
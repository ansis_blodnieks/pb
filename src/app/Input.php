<?php

namespace App;


use App\Exceptions\InvalidNumberException;
use App\Exceptions\InvalidStringException;
use App\Exceptions\NumberLessThanStringLengthException;

class Input
{
    /**
     * @var int
     */
    private $number = -1;

    /**
     * @var string
     */
    private $string = "";

    private $isInitialized = false;

    /**
     * @param int $number
     * @param string $string
     * @throws InvalidNumberException
     * @throws InvalidStringException
     * @throws NumberLessThanStringLengthException
     */
    public function init(int $number, string $string)
    {
        $this->validate($number, $string);
        $this->number = $number;
        $this->string = $string;
        $this->isInitialized = true;
    }

    /**
     * @param int $number
     * @param string $string
     * @throws InvalidNumberException
     * @throws InvalidStringException
     * @throws NumberLessThanStringLengthException
     */
    private function validate(int $number, string $string)
    {
        if ($number < 1) {
            throw new InvalidNumberException();
        }

        if (empty($string)) {
            throw new InvalidStringException();
        } elseif (strlen($string) < $number) {
            throw new NumberLessThanStringLengthException();
        }
    }

    public function getIsInitialized(): bool
    {
        return $this->isInitialized;
    }

    /**
     * @return int
     */
    public function getNumber(): int
    {
        return $this->number;
    }

    /**
     * @return string
     */
    public function getString(): string
    {
        return $this->string;
    }
}
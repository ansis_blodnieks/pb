<?php

namespace App;


use App\Exceptions\InputNotInitializedException;

class Task
{
    /**
     * @var Input
     */
    private $input;

    /**
     * @var array
     */
    private $chunks;

    public function __construct(Input $input)
    {
        $this->input = $input;
    }

    /**
     * @throws \Exception
     */
    public function execute()
    {
        if (!$this->input->getIsInitialized()) {
            throw new InputNotInitializedException();
        }

        $this->chunks = [];

        $letters = str_split($this->input->getString());

        if ($this->input->getNumber() === 1) {
            $this->chunks[] = $letters;

            if (strlen($this->input->getString()) === 1) {
                return;
            }

        } elseif (!(strlen($this->input->getString()) % $this->input->getNumber())) {
            $this->chunks[] = str_split($this->input->getString(), $this->input->getNumber());
        }

        for ($i = 0; $i < strlen($this->input->getString()); $i++) {

            $pts = [
                substr($this->input->getString(), 0, $i),
                substr($this->input->getString(), $i),
            ];

            foreach ($pts as $pt) {
                if (strlen($pt) && strlen($pt) < $this->input->getNumber()) {
                    continue 2;
                }
            }

            $this->chunks[] = array_values(array_filter($pts));
        }
    }

    public function getChunks(): array
    {
        return $this->chunks;
    }
}
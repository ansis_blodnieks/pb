<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;

final class MainTest extends TestCase
{
    public function testTaskShouldReturnOneChunkFromOneLetter()
    {
        $input = new \App\Input();
        $input->init(1, "a");

        $task = new \App\Task($input);
        $task->execute();
        $this->assertEquals(count($task->getChunks()), 1);
    }

    public function testStringShouldBeWhatIsEntered()
    {
        $input = new \App\Input();
        $input->init(1, "abc");
        $this->assertEquals($input->getString(), "abc");
    }

    public function testNumberShouldBeWhatIsEntered()
    {
        $input = new \App\Input();
        $input->init(1, "abc");
        $this->assertEquals($input->getNumber(), 1);
    }

    public function testInputMustBeInitialized()
    {
        $input = new \App\Input();
        $task = new \App\Task($input);
        $this->expectException(\App\Exceptions\InputNotInitializedException::class);
        $task->execute();
    }

    public function testValidNumberShouldBeProvided()
    {
        $this->expectException(\App\Exceptions\InvalidNumberException::class);
        $input = new \App\Input();
        $input->init(0, "a");
    }

    public function testValidStringShouldBeProvided()
    {
        $this->expectException(\App\Exceptions\InvalidStringException::class);
        $input = new \App\Input();
        $input->init(1, "");
    }

    public function testExactExample1()
    {
        $input = new \App\Input();
        $input->init(2, "abcdef");

        $expected = [
            ["abcdef"],
            ["ab", "cdef"],
            ["abcd", "ef"],
            ["abc", "def"],
            ["ab", "cd", "ef"],
        ];

        array_multisort($expected);

        $task = new \App\Task($input);
        $task->execute();

        $result = $task->getChunks();
        array_multisort($result);

        $this->assertEquals($expected, $result);
    }

    public function testExactExample2()
    {
        $input = new \App\Input();
        $input->init(1, "abc");

        $expected = [
            ["abc"],
            ["ab", "c"],
            ["a", "bc"],
            ["a", "b", "c"],
        ];

        array_multisort($expected);

        $task = new \App\Task($input);
        $task->execute();

        $result = $task->getChunks();
        array_multisort($result);

        $this->assertEquals($expected, $result);
    }
}

## Task description

Given a string and a number N (N>0), output a list of all possible combinations of dividing that string into substrings, where each substring has a length of at least N characters.

Solution for input string ‘abc’ and N=1:

`[[‘a’,’b’,‘c’], [‘ab’,‘c’], [‘a’,‘bc’], [‘abc’]]`

Notes:

- For the example above, “[‘a’,‘b’]” would NOT be a valid combination, because it does not amount to a full string ‘abc’.
- For the example above, “[‘c’,‘a’,‘b’]” is NOT a valid combination, because of incorrect order of substrings.

Bonus points for writing a unit test (or developing using TDD).

## Setup

```
composer install
```

## Executing

```
php src/index.php [number] [string]
```

## Running tests

```
./vendor/bin/phpunit --bootstrap vendor/autoload.php src/tests/MainTest
```

## Code examples

See files under `code_examples/` directory. Code if from one of current projects (not the cleanest one).